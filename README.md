Hexarotor
=========

This repo houses all the resources required for building an ACL hexarotor vehicle. See the [wiki](https://gitlab.com/mit-acl/fsw/vehicle-builds/hx/-/wikis/home) to get started.

